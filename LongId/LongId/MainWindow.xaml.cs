﻿using LongId.Comm;
using LongId.CustomControl;
using System;
using System.Windows;
using System.Windows.Input;

namespace LongId {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        private DateTime _dt;
        private uint _tid;
        
        public MainWindow() {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e) {
            _dt = DateTime.Now;
            _tid = 1;
            chkDateTimeValid.IsChecked = true;
        }

        private void UpdateUiElements() {
            tbTid.Text = _tid.ToString();
            dtPicker.SetValue(_dt);
            uint ts = (uint)DateTimeHelper.ConvertToUnixTimestamp(_dt);
            tbTs.Text = ts.ToString();
            ulong longId = (ulong)_tid << 32;
            if (chkDateTimeValid.IsChecked == true) {
                longId |= (ulong)ts;
            }
            lblResult.Content = longId.ToString();
        }

        private void CMD_MyTextBoxChanged(object sender, ExecutedRoutedEventArgs e) {
            MyTextBox tb = e.Source as MyTextBox;
            if (tb == tbTid) {
                uint tid;
                if(uint.TryParse(tb.Text, out tid)) {
                    _tid = tid;
                }
                UpdateUiElements();
            }
            if (tb == tbTs) {
                uint ts;
                if(uint.TryParse(tb.Text, out ts) && ts>0) {
                    _dt = DateTimeHelper.ConvertFromUnixTimestamp(ts);
                    
                }
                UpdateUiElements();
            }
        }

        private void CMD_MyDateTimePickerValueChanged(object sender, ExecutedRoutedEventArgs e) {
            MyDateTimePicker dtp = e.Source as MyDateTimePicker;
            if(dtp== dtPicker) {
                if(dtp.Value!= null)
                    _dt = (DateTime)dtp.Value;
                UpdateUiElements();
            }
        }

        private void CopyResult_Click(object sender, RoutedEventArgs e) {
            Clipboard.SetText(lblResult.Content as String);
        }

        private void PasteLongId_Click(object sender, RoutedEventArgs e) {
            string txt = Clipboard.GetText();
            if (txt != null) {
                ulong id;
                if(ulong.TryParse(txt, out id)) {
                    _tid = (uint)(id >> 32);
                    uint ts = (uint)(id & 0x00000000FFFFFFFF);
                    _dt = DateTimeHelper.ConvertFromUnixTimestamp(ts);
                    chkDateTimeValid.IsChecked = true;
                }
                else {
                    lblResult.Content = ":(";
                }
            }
        }

        private void chkDateTimeValid_Checked(object sender, RoutedEventArgs e) {
            dtPicker.IsEnabled = true;
            tbTs.IsEnabled = true;
            UpdateUiElements();
        }

        private void chkDateTimeValid_Unchecked(object sender, RoutedEventArgs e) {
            dtPicker.IsEnabled = false;
            tbTs.IsEnabled = false;
            UpdateUiElements();
        }
    }
}
