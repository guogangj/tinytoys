﻿using System;
using System.Windows;
using System.Windows.Input;
using Xceed.Wpf.Toolkit;

namespace LongId.CustomControl {
    public class MyDateTimePicker : DateTimePicker {

        private static readonly RoutedUICommand _datePickerValueChanged = new RoutedUICommand("DateTimePickerValueChanged",
"DateTimePickerValueChanged", typeof(MyDateTimePicker), null);

        public static RoutedCommand DateTimePickerValueChanged { get { return _datePickerValueChanged; } }

        private bool _triggerByProgram;

        public MyDateTimePicker() {
            ValueChanged += OnValueChanged;
        }

        private void OnValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e) {
            if (!_triggerByProgram) {
                _datePickerValueChanged.Execute(null, sender as MyDateTimePicker);
            }
        }

        public void SetValue(DateTime dt) {
            _triggerByProgram = true;
            Value = dt;
            _triggerByProgram = false;
        }
    }
}
