﻿using LongId.Comm;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;

namespace LongId.CustomControl {

    public class MyTextBox : TextBox {

        private static readonly RoutedUICommand _myTextBoxChanged = new RoutedUICommand("MyTextBoxChanged",
"MyTextBoxChanged", typeof(MyTextBox), null);

        public static RoutedCommand MyTextBoxChanged { get { return _myTextBoxChanged; } }

        public MyTextBox() {
            AddHandler(PreviewMouseLeftButtonDownEvent, new MouseButtonEventHandler(SelectivelyIgnoreMouseButton), true);
            //鼠标左键点击前
            AddHandler(GotKeyboardFocusEvent, new RoutedEventHandler(SelectAllText), true); //获得输入焦点
            AddHandler(MouseDoubleClickEvent, new RoutedEventHandler(SelectAllText), true); //双击
            AddHandler(LostFocusEvent, new RoutedEventHandler(OnLostFocus), true); //失去焦点
            AddHandler(KeyDownEvent, new KeyEventHandler(OnKeyDown), true); //按下某键
        }

        private static void SelectivelyIgnoreMouseButton(object sender, MouseButtonEventArgs e) {
            // Find the TextBox
            DependencyObject parent = e.OriginalSource as UIElement;
            while (parent != null && !(parent is TextBox)) {
                parent = VisualTreeHelper.GetParent(parent);
            }
            if (parent != null) {
                TextBox textBox = (TextBox)parent;
                if (!textBox.IsKeyboardFocusWithin) {
                    // If the text box is not yet focussed, give it the focus and
                    // stop further processing of this click event.
                    textBox.Focus();
                    e.Handled = true;
                }
            }
        }

        private static void SelectAllText(object sender, RoutedEventArgs e) {
            TextBox textBox = e.OriginalSource as TextBox;
            if (textBox != null)
                textBox.SelectAll();
        }

        private static void OnLostFocus(object sender, RoutedEventArgs e) {
            _myTextBoxChanged.Execute(null, sender as MyTextBox);
        }

        private static void OnKeyDown(object sender, KeyEventArgs e) {
            if (e.Key == Key.Enter) {
                _myTextBoxChanged.Execute(null, sender as MyTextBox);
            }
        }

        private async Task HideTheToolTip(ToolTip tooltip) {
            await Task.Delay(2000);
            tooltip.IsOpen = false;
            ToolTip = null;
        }

        public void ShowErrorMessage(string errMessage) {
            ToolTip tooltip = ToolTip as ToolTip;
            if (tooltip == null) {
                tooltip = new ToolTip();
                ToolTip = tooltip;
            }
            tooltip.Content = errMessage;
            tooltip.Placement = PlacementMode.Right;
            tooltip.PlacementTarget = this;
            tooltip.IsOpen = true;
            HideTheToolTip(tooltip).Forget();
        }
    }
}