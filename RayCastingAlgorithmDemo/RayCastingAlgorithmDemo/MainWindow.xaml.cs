﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RayCastingAlgorithmDemo {
    enum MousingState {
        Drawing,
        Testing
    };

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        List<Point> _thePoints = new List<Point>();
        MousingState _mousingState = MousingState.Drawing;
        Polyline _polyLine = new Polyline();
        Polygon _polygon = new Polygon();
        SolidColorBrush _blackBrush = new SolidColorBrush();
        SolidColorBrush _blueBrush = new SolidColorBrush();
        SolidColorBrush _yellowBrush = new SolidColorBrush();
        public MainWindow() {
            InitializeComponent();
        }

        private void theCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
            Point pt = e.GetPosition(theCanvas);
            if (_mousingState == MousingState.Drawing) {
                _thePoints.Add(pt);
                if (e.ClickCount == 2) {
                    _mousingState = MousingState.Testing;
                }
                Refresh();
            }
            else {
                if (RayCastingAlgorithm.IsWithin(pt, _thePoints, cbNoneZeroMode.IsChecked==true)) {
                    _polygon.Fill = _yellowBrush;
                }
                else {
                    _polygon.Fill = _blueBrush;
                }
            }
        }

        private void NewButton_Click(object sender, RoutedEventArgs e) {
            _thePoints.Clear();
            _mousingState = MousingState.Drawing;
            _polygon.Fill = _blueBrush;
            Refresh();
        }

        private void Refresh() {
            if (_mousingState == MousingState.Testing) {
                _polygon.Visibility = Visibility.Visible;
                _polyLine.Visibility = Visibility.Hidden;
            }
            else {
                _polygon.Visibility = Visibility.Hidden;
                _polyLine.Visibility = Visibility.Visible;
                _polyLine.Points = new PointCollection(_thePoints);
                _polygon.Points = new PointCollection(_thePoints);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e) {
            _blackBrush.Color = Colors.Black;
            _blueBrush.Color = Colors.Blue;
            _yellowBrush.Color = Colors.Yellow;

            _polyLine.Stroke = _blackBrush;
            _polyLine.StrokeThickness = 1;
            _polygon.Stroke = _blackBrush;
            _polygon.StrokeThickness = 1;
            _polygon.Fill = _blueBrush;
            _polygon.FillRule = FillRule.EvenOdd;

            theCanvas.Children.Add(_polyLine);
            theCanvas.Children.Add(_polygon);
        }

        private void cbNoneZeroMode_Click(object sender, RoutedEventArgs e) {
            if (cbNoneZeroMode.IsChecked == true) {
                _polygon.FillRule = FillRule.Nonzero;
            }
            else {
                _polygon.FillRule = FillRule.EvenOdd;
            }
        }
    }
}
