﻿using System;
using System.IO;
using System.Net.Security;
using System.Net.Sockets;
using System.Reflection;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Threading;

namespace TcpRunner.Network {

    public enum OperationResult {
        Succeeded,
        Disconnected,
        Error
    }

    public delegate void OnConnectCallback(bool succeeded, string errorMessage);

    //Make sure the data is no more used after this function return
    public delegate void OnReadCallback(OperationResult result, string errorMessage, byte[] data, int size);

    public delegate void OnSendCallback(bool succeeded, string errorMessage);

    public class TcpAdaptor {
        private bool _needToHandleReadCallback = false;
        const int ReadBufferSize = 8192;

        private TcpClient _tcpClient = null;
        private bool _useTls12 = false;
        private Stream _stream = null;
        private string _address = null;
        private byte[] _readBuff = new byte[ReadBufferSize];
        private ManualResetEvent _sslAuthTimeoutEvent = new ManualResetEvent(false); //Turns signaled when SSL authentication is timeout.

        public bool IsConnected {
            get {
                return _stream != null;
            }
        }

        private void TryToGetErrorInfoFromAsyncResult(IAsyncResult ar, out bool succeeded, out string errorMessage) {
            succeeded = true;
            errorMessage = null;
            //Try to get exception information
            PropertyInfo resultProperty = ar.GetType().GetProperty("Result", BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.NonPublic);
            if (resultProperty != null) {
                SocketException exception = resultProperty.GetValue(ar) as SocketException;
                if (exception != null) {
                    succeeded = false;
                    errorMessage = exception.Message;
                }
            }
        }

        private bool ValidateServerCertificateCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) {
            /*if (sslPolicyErrors == SslPolicyErrors.None)
                return true;
            return false;*/
            return true;
        }

        private void SslAuthenticateAsClientCallback(IAsyncResult ar) {
            if (!_needToHandleReadCallback) {
                return;
            }
            _sslAuthTimeoutEvent.Set();
            bool succeeded = true;
            string errorMessage = null;
            TryToGetErrorInfoFromAsyncResult(ar, out succeeded, out errorMessage);
            try {
                ((SslStream)_stream).EndAuthenticateAsClient(ar);
            }
            catch (Exception ex) {
                if (errorMessage == null) {
                    errorMessage = ex.Message;
                }
                succeeded = false;
            }
            OnConnectCallback callerCb = ar.AsyncState as OnConnectCallback;
            if (callerCb != null) {
                callerCb(succeeded, errorMessage);
            }
        }

        public void SslAuthCompleteOrTimeoutCallback(object state, bool timeout){
            if (timeout) {
                CloseConnect();
                OnConnectCallback clientCb = state as OnConnectCallback;
                if (clientCb != null) {
                    clientCb(false, "TLS authentication timeout.");
                }
            }
        }

        private void ConnectCallback(IAsyncResult ar) {
            bool succeeded = true;
            string errorMessage = null;
            TryToGetErrorInfoFromAsyncResult(ar, out succeeded, out errorMessage);
            try {
                _tcpClient.EndConnect(ar);
            }
            catch (Exception ex) {
                if (errorMessage == null) {
                    errorMessage = ex.Message;
                }
                succeeded = false;
            }

            _needToHandleReadCallback = succeeded;

            bool bNeedCallback = true;
            if (_useTls12) {
                if (succeeded) {
                    SslStream sslStream = new SslStream(_tcpClient.GetStream(), false, new RemoteCertificateValidationCallback(ValidateServerCertificateCallback), null);
                    _stream = sslStream;
                    _sslAuthTimeoutEvent.Reset();
                    ThreadPool.RegisterWaitForSingleObject(_sslAuthTimeoutEvent, SslAuthCompleteOrTimeoutCallback, ar.AsyncState, 5000, true);
                    sslStream.BeginAuthenticateAsClient(_address, null, SslProtocols.Tls12, false, SslAuthenticateAsClientCallback, ar.AsyncState);
                    bNeedCallback = false;
                }
            }
            else {
                if (succeeded) {
                    _stream = _tcpClient.GetStream();
                }
            }
            if (bNeedCallback) {
                //Call the client's callback function
                OnConnectCallback callerCb = ar.AsyncState as OnConnectCallback;
                if (callerCb != null) {
                    callerCb(succeeded, errorMessage);
                }
            }
        }

        public void BeginConnect(string address, ushort port, OnConnectCallback cb, bool useTls12 = false) {
            CloseConnect();
            _useTls12 = useTls12;
            _tcpClient = new TcpClient(AddressFamily.InterNetwork);
            _address = address;
            _tcpClient.BeginConnect(address, port, ConnectCallback, cb);
        }

        public void CloseConnect() {
            _needToHandleReadCallback = false;
            if (_stream != null) {
                _stream.Close();
                _stream = null;
            }
            if (_tcpClient != null) {
                _tcpClient.Close();
                _tcpClient = null;
            }
        }

        private void ReadCallback(IAsyncResult ar) {
            if (!_needToHandleReadCallback)
                return;
            OperationResult result = OperationResult.Succeeded;
            string errorMessage = null;
            bool succeeded;
            int bytesRead = 0;
            TryToGetErrorInfoFromAsyncResult(ar, out succeeded, out errorMessage);
            try {
                bytesRead = _stream.EndRead(ar);
            }
            catch (Exception ex) {
                if (errorMessage == null) {
                    errorMessage = ex.Message;
                }
                succeeded = false;
            }
            if (!succeeded) {
                result = OperationResult.Error;
            }

            if (result != OperationResult.Error && bytesRead == 0) {
                result = OperationResult.Disconnected;
            }

            if (result != OperationResult.Succeeded) {
                CloseConnect();
            }

            //Call the client's callback function
            OnReadCallback callerCb = ar.AsyncState as OnReadCallback;
            if (callerCb != null) {
                callerCb(result, errorMessage, _readBuff, bytesRead);
            }

            //Continue next read
            if (result == OperationResult.Succeeded) {
                _stream.BeginRead(_readBuff, 0, ReadBufferSize, new AsyncCallback(ReadCallback), callerCb);
            }
        }

        //Call this after connection established
        public void BeginRead(OnReadCallback cb) {
            if (!IsConnected)
                return;
            
            _stream.BeginRead(_readBuff, 0, ReadBufferSize, new AsyncCallback(ReadCallback), cb);
        }

        private void SendCallback(IAsyncResult ar) {
            string errorMessage = null;
            bool succeeded;
            TryToGetErrorInfoFromAsyncResult(ar, out succeeded, out errorMessage);
            try {
                _stream.EndWrite(ar);
            }
            catch (Exception ex) {
                if (errorMessage == null) {
                    errorMessage = ex.Message;
                }
                succeeded = false;
            }

            //Call the client's callback function
            OnSendCallback callerCb = ar.AsyncState as OnSendCallback;
            if (callerCb != null) {
                callerCb(succeeded, errorMessage);
            }
        }

        public void BeginSend(byte[] data, int size, OnSendCallback cb) {
            if (!IsConnected)
                return;
            _stream.BeginWrite(data, 0, size, new AsyncCallback(SendCallback), cb);
        }
    }
}
