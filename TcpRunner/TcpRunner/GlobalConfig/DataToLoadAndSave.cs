﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace TcpRunner.GlobalConfig {
    public class DataToLoadAndSave {

        public int LastUsedServerIndex { get; set; }
        public List<TcpServerInfo> TcpServerInfo { get; set; }

        public static DataToLoadAndSave Load() {
            try {
                using (StreamReader reader = new StreamReader(Consts.TheConfigFileFullPath)) {
                    XmlSerializer serializer = new XmlSerializer(typeof(DataToLoadAndSave));
                    return (DataToLoadAndSave)serializer.Deserialize(reader);
                }
            }
            catch (Exception) {
                DataToLoadAndSave data = new DataToLoadAndSave {
                    LastUsedServerIndex = 0,
                    TcpServerInfo = new List<TcpServerInfo>()
                };
                return data;
            }
        }

        public void Save() {
            using (StreamWriter writer = new StreamWriter(Consts.TheConfigFileFullPath)) {
                XmlSerializer serializer = new XmlSerializer(typeof(DataToLoadAndSave));
                serializer.Serialize(writer, this);
            }
        }
    }
}
