﻿namespace TcpRunner.GlobalConfig {
    public class TcpServerInfo {
        public string Name { get; set; }
        public string Address { get; set; }
        public ushort Port { get; set; }
        public bool Tls12 { get; set; }
    }
}
