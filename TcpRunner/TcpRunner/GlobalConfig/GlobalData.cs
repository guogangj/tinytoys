﻿using System.Collections.Generic;

namespace TcpRunner.GlobalConfig {

    public enum NetworkState {
        Disconnected,
        Connecting,
        Connected,
        Disconnecting
    }

    public class GlobalData {
        private DataToLoadAndSave _dataToLoadAndSave;
        private NetworkState _networkState = NetworkState.Disconnected;

        private static GlobalData _singleton = null;

        public GlobalData() {
            _dataToLoadAndSave = DataToLoadAndSave.Load();
        }

        public static GlobalData Singleton {
            get {
                if (_singleton == null) {
                    _singleton = new GlobalData();
                }
                return _singleton;
            }
        }

        public List<TcpServerInfo> TcpServerInfo {
            get {
                return _dataToLoadAndSave.TcpServerInfo;
            }
        }

        public void AddTcpServer(TcpServerInfo serverInfo) {
            _dataToLoadAndSave.TcpServerInfo.Add(serverInfo);
            _dataToLoadAndSave.Save();
        }

        public void EditTcpServer(TcpServerInfo serverInfo, int index) {
            if (index < _dataToLoadAndSave.TcpServerInfo.Count) {
                _dataToLoadAndSave.TcpServerInfo[index] = serverInfo;
                _dataToLoadAndSave.Save();
            }
        }

        public void RemoveTcpServer(int index) {
            if (index < _dataToLoadAndSave.TcpServerInfo.Count) {
                _dataToLoadAndSave.TcpServerInfo.RemoveAt(index);
                _dataToLoadAndSave.Save();
            }
            _dataToLoadAndSave.Save();
        }

        public int LastUsedServerIndex {
            get {
                return _dataToLoadAndSave.LastUsedServerIndex;
            }
            set {
                _dataToLoadAndSave.LastUsedServerIndex = value;
                _dataToLoadAndSave.Save();
            }
        }

        public NetworkState NwStat {
            get { return _networkState; }
            set { _networkState = value; }
        }
    }
}
