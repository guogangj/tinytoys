﻿using System;

namespace TcpRunner.Data {
    public enum DataItemType {
        Connect,
        Disconnect,
        Receive,
        Send
    }

    public class DataItem {
        public DataItemType Type { get; set; } //Message type
        public DateTime OccurDt { get; set; } //Occur datetime
        public int CompleteInMs { get; set; } //Time elapsed from Occurdt in milliseconds 
        public bool? Result { get; set; } //Complete state
        public string DataDisp { get; set; } //Data Converted to UTF-8 string for displaying in the listview
        public byte[] DataRaw { get; set; } //Raw data. It has a maximum limitation
    }
}
