﻿using System.Windows;
using System.Windows.Input;

namespace TcpRunner {

    public partial class AddOrEditTcpServer : Window {

        public string ServerName { get; set; }
        public string ServerAddress { get; set; }
        public ushort ServerPort { get; set; }
        public bool UseTls12 { get; set; }

        public AddOrEditTcpServer() {
            InitializeComponent();
        }

        private void Window_PreviewKeyDown(object sender, KeyEventArgs e) {
            if (e.Key == Key.Escape)
                DialogResult = false;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e) {
            ServerName = tbName.Text;
            ServerAddress = tbAddress.Text;
            ServerPort = ushort.Parse(tbPort.Text);
            UseTls12 = cbUseTls12.IsChecked == true;
            DialogResult = true;
        }

        private void UpdateOkButtonState() {
            ushort port;
            if(!ushort.TryParse(tbPort.Text, out port)
                || string.IsNullOrEmpty(tbName.Text)
                || string.IsNullOrEmpty(tbAddress.Text)) {
                btnOk.IsEnabled = false;
            }
            else {
                btnOk.IsEnabled = true;
            }
        }

        private void CMD_MyTextBoxChanged(object sender, ExecutedRoutedEventArgs e) {
            UpdateOkButtonState();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e) {
            tbName.Text = ServerName;
            tbAddress.Text = ServerAddress;
            if(ServerPort>0) tbPort.Text = ServerPort.ToString();
            cbUseTls12.IsChecked = UseTls12;
            UpdateOkButtonState();
            tbName.Focus();
        }
    }
}
