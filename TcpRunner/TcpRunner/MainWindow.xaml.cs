﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using TcpRunner.GlobalConfig;
using TcpRunner.Network;
using TcpRunner.Data;
using System.Windows.Input;
using System.Windows.Controls;
using TcpRunner.Comm;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Data;

namespace TcpRunner {

    public partial class MainWindow : Window {

        private string _connectingAddress; //For display
        private ushort _connectingPort; //For display
        private List<DataItem> _totalData = new List<DataItem>();
        private const string StrHexOnly = "(Hex Only)";
        private const string StrEmpty = "(Empty)";
        private TcpAdaptor _tcpAdaptor = new TcpAdaptor();
        private bool _isCharDispValid = false;
        private bool _isHexDispValid = false;
        public MainWindow() {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e) {
            comboTcpServers.ItemsSource = GlobalData.Singleton.TcpServerInfo;
            comboTcpServers.SelectedIndex = GlobalData.Singleton.LastUsedServerIndex < GlobalData.Singleton.TcpServerInfo.Count ? GlobalData.Singleton.LastUsedServerIndex : GlobalData.Singleton.TcpServerInfo.Count - 1;
            lvMessage.ItemsSource = _totalData;
            UpdateUiElements();
            
            DataObject.AddPastingHandler(tbInput, OnInputTextBoxPasted);
        }

        private void UpdateUiElements() {
            if (GlobalData.Singleton.NwStat == NetworkState.Disconnected) {
                btnAddServer.IsEnabled = true;
                comboTcpServers.IsEnabled = GlobalData.Singleton.TcpServerInfo.Count > 0;
                btnConnect.IsEnabled = comboTcpServers.SelectedIndex >= 0;
                btnRemoveServer.IsEnabled = comboTcpServers.SelectedIndex >= 0;
                btnEditServer.IsEnabled = comboTcpServers.SelectedIndex >= 0;
            }
            else {
                btnAddServer.IsEnabled = false;
                comboTcpServers.IsEnabled = false;
                btnConnect.IsEnabled = false;
                btnRemoveServer.IsEnabled = false;
                btnEditServer.IsEnabled = false;
                connectedIndicator.Visibility = Visibility.Collapsed;
            }

            if (GlobalData.Singleton.NwStat == NetworkState.Connected) {
                connectedIndicator.Visibility = Visibility.Visible;
            }
            else {
                connectedIndicator.Visibility = Visibility.Collapsed;
            }

            btnSend.IsEnabled = GlobalData.Singleton.NwStat == NetworkState.Connected;

            if (GlobalData.Singleton.NwStat == NetworkState.Connecting) {
                btnConnect.Content = "Connecting...";
            }
            else {
                btnConnect.Content = "Connect";
            }

            if (GlobalData.Singleton.NwStat == NetworkState.Disconnected || GlobalData.Singleton.NwStat == NetworkState.Connecting) {
                btnConnect.Visibility = Visibility.Visible;
                btnDisconnect.Visibility = Visibility.Collapsed;
            }
            else {
                btnConnect.Visibility = Visibility.Collapsed;
                btnDisconnect.Visibility = Visibility.Visible;
            }
            btnReformatHex.IsEnabled = cbHex.IsChecked == true;
        }

        private void AddTcpServerButton_Click(object sender, RoutedEventArgs e) {
            AddOrEditTcpServer dlg = new AddOrEditTcpServer();
            dlg.Title = "Add TCP Server";
            dlg.ShowInTaskbar = false;
            dlg.Owner = this;
            if (dlg.ShowDialog() == true) {
                GlobalData.Singleton.AddTcpServer(new TcpServerInfo {
                    Name = dlg.ServerName,
                    Address = dlg.ServerAddress,
                    Port = dlg.ServerPort,
                    Tls12 = dlg.UseTls12
                });
                comboTcpServers.Items.Refresh();
                comboTcpServers.SelectedIndex = comboTcpServers.Items.Count - 1;
                UpdateUiElements();
            }
        }

        private void RemoveServerButton_Click(object sender, RoutedEventArgs e) {
            int selectedIndex = comboTcpServers.SelectedIndex;
            GlobalData.Singleton.RemoveTcpServer(selectedIndex);
            if (selectedIndex > GlobalData.Singleton.TcpServerInfo.Count - 1) {
                selectedIndex = GlobalData.Singleton.TcpServerInfo.Count - 1;
            }
            comboTcpServers.Items.Refresh();
            comboTcpServers.SelectedIndex = selectedIndex;
            UpdateUiElements();
        }

        private void EditServerButton_Click(object sender, RoutedEventArgs e) {
            TcpServerInfo serverInfo = comboTcpServers.SelectedValue as TcpServerInfo;
            if (serverInfo == null) {
                return;
            }
            AddOrEditTcpServer dlg = new AddOrEditTcpServer();
            dlg.Title = "Edit TCP Server";
            dlg.ShowInTaskbar = false;
            dlg.Owner = this;
            dlg.ServerName = serverInfo.Name;
            dlg.ServerAddress = serverInfo.Address;
            dlg.ServerPort = serverInfo.Port;
            dlg.UseTls12 = serverInfo.Tls12;
            if (dlg.ShowDialog() == true) {
                int selectedIndex = comboTcpServers.SelectedIndex;
                GlobalData.Singleton.EditTcpServer(new TcpServerInfo {
                    Name = dlg.ServerName,
                    Address = dlg.ServerAddress,
                    Port = dlg.ServerPort,
                    Tls12 = dlg.UseTls12
                }, comboTcpServers.SelectedIndex);
                comboTcpServers.Items.Refresh();
                comboTcpServers.SelectedIndex = selectedIndex;
                UpdateUiElements();
            }
        }

        private void UpdateCloseUi() {
            _tcpAdaptor.CloseConnect();
            GlobalData.Singleton.NwStat = NetworkState.Disconnected;
            _totalData.Add(new DataItem() { Type = DataItemType.Disconnect, OccurDt = DateTime.Now, CompleteInMs = 0, Result = true, DataRaw = null, DataDisp = "Disconnected from " + _connectingAddress + ":" + _connectingPort });
            RefreshMessageList();
        }

        private string ReduceStrForDisp(string org, bool isHex=false) {
            if (isHex) {
                org = StringUtils.HexToText(org);
            }
            if (org.Length > 100) {
                return org.Substring(0, 100);
            }
            return org;
        }

        //This method is called by a thread in pool...
        private void TcpReadCallback(OperationResult result, string errorMessage, byte[] data, int size) {
            DataItem itemToAdd = null;
            if (result == OperationResult.Succeeded) {
                string strDisp;
                try {
                    strDisp = Encoding.UTF8.GetString(data, 0, size);
                }
                catch (Exception) {
                    strDisp = StrHexOnly;
                }
                strDisp = ReduceStrForDisp(strDisp);
                byte[] dataRaw = new byte[size];
                Buffer.BlockCopy(data, 0, dataRaw, 0, size);
                itemToAdd = new DataItem() { Type = DataItemType.Receive, OccurDt = DateTime.Now, CompleteInMs = 0, Result = true, DataRaw = dataRaw, DataDisp = strDisp };
            }
            Dispatcher.BeginInvoke((Action)(() => {
                if (result == OperationResult.Succeeded) {
                    _totalData.Add(itemToAdd);
                    RefreshMessageList();
                }
                else if (result == OperationResult.Disconnected) {
                    UpdateCloseUi();
                }
                else { //Error
                    UpdateCloseUi();
                    MessageBox.Show(this, errorMessage, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                UpdateUiElements();
            }));
        }

        private void RefreshMessageList() {
            lvMessage.Items.Refresh();
            if(lvMessage.Items.Count>0)
                lvMessage.ScrollIntoView(lvMessage.Items[lvMessage.Items.Count - 1]);
        }

        private DataItem FindFirstPaddingDataItem(DataItemType type) {
            return _totalData.FirstOrDefault(i => i.Result == null && i.Type == type);
        }

        private void UpdateConnnectDataItemInfo(bool succeeded) {
            DataItem item = FindFirstPaddingDataItem(DataItemType.Connect);
            if (item != null) {
                item.Result = succeeded;
                item.CompleteInMs = (int)((DateTime.Now - item.OccurDt).TotalMilliseconds);
                RefreshMessageList();
            }
        }

        //This method is called by a thread in pool, so use BeginInvoke to interact with UI
        private void TcpConnectCallback(bool succeeded, string errorMessage) {
            GlobalData.Singleton.NwStat = succeeded ? NetworkState.Connected : NetworkState.Disconnected;
            Dispatcher.BeginInvoke((Action)(() => {
                UpdateConnnectDataItemInfo(succeeded);
                UpdateUiElements();
                if (!succeeded) {
                    MessageBox.Show(this, errorMessage, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else {
                    GlobalData.Singleton.LastUsedServerIndex = comboTcpServers.SelectedIndex;
                    _tcpAdaptor.BeginRead(TcpReadCallback);
                }
            }));
        }

        private void ConnectButton_Click(object sender, RoutedEventArgs e) {
            TcpServerInfo serverInfo = comboTcpServers.SelectedValue as TcpServerInfo;
            if (serverInfo == null) {
                return;
            }
            GlobalData.Singleton.NwStat = NetworkState.Connecting;
            _connectingAddress = serverInfo.Address;
            _connectingPort = serverInfo.Port;
            UpdateUiElements();
            _tcpAdaptor.BeginConnect(serverInfo.Address, serverInfo.Port, TcpConnectCallback, serverInfo.Tls12);
            _totalData.Add(new DataItem {
                Type = DataItemType.Connect,
                OccurDt = DateTime.Now,
                CompleteInMs = 0,
                Result = null,
                DataDisp = "Connect to " + _connectingAddress + ":" + _connectingPort,
                DataRaw = null
            });
            RefreshMessageList();
        }

        private void DisconnectButton_Click(object sender, RoutedEventArgs e) {
            _tcpAdaptor.CloseConnect();
            UpdateCloseUi();
            UpdateUiElements();
        }

        protected void ListViewItem_Clicked(object sender, MouseButtonEventArgs e) {
            DataItem item = (DataItem)((ListViewItem)sender).Content;
            string strChar;
            string strHex;


            if (item.Type == DataItemType.Receive || item.Type == DataItemType.Send) {
                try {
                    strChar = Encoding.UTF8.GetString(item.DataRaw, 0, item.DataRaw.Length);
                    _isCharDispValid = true;
                }
                catch (Exception) {
                    strChar = StrHexOnly;
                    _isCharDispValid = false;
                }


                if (item.DataRaw == null || item.DataRaw.Length == 0) {
                    strHex = StrEmpty;
                    _isHexDispValid = false;
                }
                else {
                    strHex = StringUtils.ByteArrayToString(item.DataRaw);
                    _isHexDispValid = true;
                }
            }
            else {
                _isCharDispValid = false;
                _isHexDispValid = false;
                strChar = item.DataDisp;
                strHex = StrEmpty;
            }
            tbChar.Text = strChar;
            tbHex.Text = strHex;
        }

        private void ListView_RightClicked(object sender, System.Windows.Input.MouseButtonEventArgs e) {
            ContextMenu contextMenu = new ContextMenu();
            MenuItem menuItem = new MenuItem();
            menuItem.Header = "Clear All";
            menuItem.Click += MenuItem_ClearAll_Clicked;
            contextMenu.Items.Add(menuItem);
            contextMenu.IsOpen = true;
        }

        private void MenuItem_ClearAll_Clicked(object sender, RoutedEventArgs e) {
            _totalData.Clear();
            RefreshMessageList();
            tbChar.Clear();
            tbChar.Select(0, 0);
            tbHex.Clear();
            tbHex.Select(0, 0);
        }

        private void tbChar_SelectionChanged(object sender, RoutedEventArgs e) {
            if (_isCharDispValid && _isHexDispValid) {
                int selStartStr = tbChar.SelectionStart;
                int selStartByte = 0;
                if (selStartStr > 0) {
                    selStartByte = Encoding.UTF8.GetBytes(tbChar.Text.Substring(0, selStartStr)).Length;
                }
                int selContentByte = 0;
                if (tbChar.SelectedText.Length > 0) {
                    selContentByte = Encoding.UTF8.GetBytes(tbChar.SelectedText).Length;
                }
                int selStartHex = selStartByte * 3;
                int selContentHex = selContentByte > 0 ? (selContentByte * 3 - 1) : 0;
                tbHex.Select(selStartHex, selContentHex);
                Dispatcher.BeginInvoke((Action)(() => { Keyboard.Focus(tbHex); Keyboard.Focus(tbChar); }));
            }
        }

        private void UpdateSendDataItemInfo(bool succeeded) {
            DataItem item = FindFirstPaddingDataItem(DataItemType.Send);
            if (item != null) {
                item.Result = succeeded;
                item.CompleteInMs = (int)((DateTime.Now - item.OccurDt).TotalMilliseconds);
                RefreshMessageList();
            }
        }

        //This method is called by a thread in pool, so use BeginInvoke to interact with UI
        private void TcpSendCallback(bool succeeded, string errorMessage) {
            Dispatcher.BeginInvoke((Action)(() => {
                UpdateSendDataItemInfo(succeeded);
                UpdateUiElements();
                if (!succeeded) {
                    MessageBox.Show(this, errorMessage, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else {
                    Console.WriteLine("Send succeeded!");
                }
            }));
        }

        private void Send_Click(object sender, RoutedEventArgs e) {
            if (tbInput.Text.Length == 0)
                return;
            byte[] bytesToSend;
            if (cbHex.IsChecked==true) {
                bytesToSend = StringUtils.HexToBytes(tbInput.Text);
            }
            else {
                bytesToSend = Encoding.UTF8.GetBytes(tbInput.Text);
            }
            _tcpAdaptor.BeginSend(bytesToSend, bytesToSend.Length, TcpSendCallback);
            _totalData.Add(new DataItem {
                Type = DataItemType.Send,
                OccurDt = DateTime.Now,
                CompleteInMs = 0,
                Result = null,
                DataDisp = ReduceStrForDisp(tbInput.Text, cbHex.IsChecked==true),
                DataRaw = bytesToSend
            });
            RefreshMessageList();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
            _tcpAdaptor.CloseConnect();
            UpdateCloseUi();
        }

        private void cbHex_Checked(object sender, RoutedEventArgs e) {
            tbInput.Text = StringUtils.TextToHex(tbInput.Text);
            UpdateUiElements();
            
        }

        private void cbHex_Unchecked(object sender, RoutedEventArgs e) {
            tbInput.Text = StringUtils.HexToText(tbInput.Text);
            UpdateUiElements();
        }

        static Regex _sRegExHex = new Regex("^[A-F|a-f|0-9|\\s]+$");
        private static bool IsHexText(string text) {
            return _sRegExHex.IsMatch(text);
        }

        private void OnInputTextBoxPasted(object sender, DataObjectPastingEventArgs e) {
            if (cbHex.IsChecked != true)
                return;
            if (e.DataObject.GetDataPresent(typeof(string))) {
                string text = (string)e.DataObject.GetData(typeof(string));
                if (!IsHexText(text)) {
                    e.CancelCommand();
                }
            }
            else {
                e.CancelCommand();
            }
        }

        private void tbInput_PreviewTextInput(object sender, TextCompositionEventArgs e) {
            if (cbHex.IsChecked != true)
                return;
            e.Handled = !IsHexText(e.Text);
        }

        private void btnReformatHex_Click(object sender, RoutedEventArgs e) {
            tbInput.Text = StringUtils.ReformatHex(tbInput.Text);
        }
    }
}
