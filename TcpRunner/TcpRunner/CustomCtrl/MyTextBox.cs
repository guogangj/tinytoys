﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using TcpRunner.Comm;

namespace TcpRunner.CustomCtrl {
    public class MyTextBox : TextBox {
        private static readonly RoutedUICommand _myTextBoxChanged = new RoutedUICommand("MyTextBoxChanged",
"MyTextBoxChanged", typeof(MyTextBox), null);

        //Will be fired when textbox's content is changed.
        public static RoutedUICommand MyTextBoxChanged {
            get { return _myTextBoxChanged; }
        }

        public MyTextBox() {
            AddHandler(PreviewMouseLeftButtonDownEvent, new MouseButtonEventHandler(SelectivelyIgnoreMouseButton), true); //加上这个，否则通过鼠标单击Focus将无法获得全部选中效果
        }
        
        private static void SelectivelyIgnoreMouseButton(object sender, MouseButtonEventArgs e) {
            // Find the TextBox
            DependencyObject parent = e.OriginalSource as UIElement;
            while (parent != null && !(parent is TextBox)) {
                parent = VisualTreeHelper.GetParent(parent);
            }
            if (parent != null) {
                TextBox textBox = (TextBox)parent;
                if (!textBox.IsKeyboardFocusWithin) {
                    // If the text box is not yet focussed, give it the focus and
                    // stop further processing of this click event.
                    textBox.Focus();
                    e.Handled = true;
                }
            }
        }

        protected override void OnGotKeyboardFocus(KeyboardFocusChangedEventArgs e) {
            base.OnGotKeyboardFocus(e);
            SelectAll();
        }

        protected override void OnTextChanged(TextChangedEventArgs e) {
            base.OnTextChanged(e);
            MyTextBoxChanged.Execute(null, this);
        }

        private async Task HideTheToolTip(ToolTip tooltip) {
            await Task.Delay(2000);
            tooltip.IsOpen = false;
            ToolTip = null;
        }

        public void ShowErrorMessage(string errMessage) {
            ToolTip tooltip = ToolTip as ToolTip;
            if (tooltip == null) {
                tooltip = new ToolTip();
                ToolTip = tooltip;
            }
            tooltip.Content = errMessage;
            tooltip.Foreground = Brushes.Red;
            tooltip.Placement = PlacementMode.Right;
            tooltip.PlacementTarget = this;
            tooltip.IsOpen = true;
            HideTheToolTip(tooltip).Forget();
        }
    }
}
