﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TcpRunner.Comm {
    public class StringUtils {
        public static string ByteArrayToString(byte[] bytes) {
            StringBuilder hex = new StringBuilder(bytes.Length * 3);
            foreach (byte b in bytes)
                hex.AppendFormat("{0:X2} ", b);
            return hex.ToString();
        }

        public static string TextToHex(string text) {
            return ByteArrayToString(Encoding.UTF8.GetBytes(text));
        }

        private static int GetValueFromTwoChars(char c) {
            if(c>='0' && c <= '9') {
                return c - '0';
            }
            if(c>='a' && c <= 'z') {
                return c - 'a' + 10;
            }
            if(c>='A' && c <= 'Z') {
                return c - 'A' + 10;
            }
            return -1;
        }

        public static byte[] HexToBytes(string hex) {
            char[] chars = hex.ToCharArray();
            List<byte> bytes = new List<byte>();
            char TheDumbValue = 'N';
            char firstChar = TheDumbValue;
            for (int i = 0; i < chars.Length; i++) {
                char currChar = chars[i];
                int charVal = GetValueFromTwoChars(currChar);
                if (charVal != -1) {
                    if (firstChar == TheDumbValue) {
                        firstChar = currChar;
                    }
                    else {
                        bytes.Add((byte)(GetValueFromTwoChars(firstChar) * 16 + GetValueFromTwoChars(currChar)));
                        firstChar = TheDumbValue;
                    }
                }
            }
            return bytes.ToArray();
        }

        public static string HexToText(string hex) {
            return Encoding.UTF8.GetString(HexToBytes(hex));
        }

        public static string ReformatHex(string hex) {
            char[] chars = hex.ToCharArray();
            StringBuilder sb = new StringBuilder();
            
            for(int i=0; i<chars.Length; i++) {
                char c = chars[i];
                if(c>='0' && c<='9' || c>='a' && c<='z' || c>='A' && c <= 'Z') {
                    sb.Append(c);
                }
                if ((sb.Length % 3) == 2) {
                    sb.Append(' ');
                }
            }
            return sb.ToString().ToUpper();
        }
    }
}
