#include "stdafx.h"
#include "SysInfo.h"
#include <strsafe.h>
#include <Windows.h>

typedef void (WINAPI *PGNSI)(LPSYSTEM_INFO);
typedef BOOL(WINAPI *PGPI)(DWORD, DWORD, DWORD, DWORD, PDWORD);

CSysInfo::CSysInfo()
{
}

CSysInfo::~CSysInfo()
{
}

SystemInfoDef* CSysInfo::GetAll()
{
	GetDotNetFramework();
	GetOperatingSystem();
	return &m_info;
}

WCHAR* CSysInfo::GetFxVersionGreaterThen4dot5(DWORD release) const {
	switch (release) {
	case 378389: //4.5
		return L".NET Framework 4.5";
	case 378675: //4.5.1 installed with Windows 8.1 or Windows Server 2012 R2
	case 378758: //4.5.1 installed on Windows 8, Windows 7 SP1, or Windows Vista SP2
		return L".NET Framework 4.5.1";
	case 379893: //4.5.2
		return L".NET Framework 4.5.2";
	case 393295: //4.6 on Windows 10
	case 393297: //4.6 on all other OS
		return L".NET Framework 4.6";
	case 394254: //4.6.1 on Windows 10 November Update systems
	case 394271: //4.6.1 on all other OS versions
		return L".NET Framework 4.6.1";
	case 394802: //4.6.2 on Windows 10 Anniversary Update
	case 394806: //4.6.2 on on all other OS versions
		return L".NET Framework 4.6.2";
	default:     //Unknown
		return L".NET Framework > 4.6.2?";
	}
}

void CSysInfo::GetDotNetFramework()
{
	DWORD dwInstall;
	DWORD dwRelease;
	DWORD dwSP;
	DWORD dwSize;
	HKEY hkResult;

	//��� HKLM\SOFTWARE\Microsoft\NET Framework Setup\NDP\v1.1.4322\Install - 1
	//         else if(HKLM\SOFTWARE\Microsoft\NET Framework Setup\NDP\v1.1.4322\SP - 1)
	//             => .NET Framework 1.1 Service Pack 1
	//         else
	//             => .NET Framework 1.1
	if (ERROR_SUCCESS == RegOpenKeyExW(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v1.1.4322", 0, KEY_READ, &hkResult))
	{
		dwSize = sizeof(dwInstall);
		if (ERROR_SUCCESS == RegQueryValueExW(hkResult, L"Install", 0, 0, (LPBYTE)&dwInstall, &dwSize) && 1 == dwInstall)
		{
			if (ERROR_SUCCESS == RegQueryValueExW(hkResult, L"SP", 0, 0, (LPBYTE)&dwSP, &dwSize) && 1 == dwSP)
			{
				StringCchCatW(m_info.szDotNetFramework, DOTNET_INFO_LENGTH, L".NET Framework 1.1 Service Pack 1\r\n");
			}
			else
			{
				StringCchCatW(m_info.szDotNetFramework, DOTNET_INFO_LENGTH, L".NET Framework 1.1\r\n");
			}
		}
		RegCloseKey(hkResult);
	}

	//��� HKLM\SOFTWARE\Microsoft\NET Framework Setup\NDP\v2.0.50727\Install - 1
	//         if(HKLM\SOFTWARE\Microsoft\NET Framework Setup\NDP\v2.0.50727\SP - 2)
	//             => .NET Framework 2.0 Service Pack 2
	//         else if(HKLM\SOFTWARE\Microsoft\NET Framework Setup\NDP\v2.0.50727\SP - 1)
	//             => .NET Framework 2.0 Service Pack 1
	//         else
	//             => .NET Framework 2.0
	if (ERROR_SUCCESS == RegOpenKeyExW(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v2.0.50727", 0, KEY_READ, &hkResult))
	{
		dwSize = sizeof(dwInstall);
		if (ERROR_SUCCESS == RegQueryValueExW(hkResult, L"Install", 0, 0, (LPBYTE)&dwInstall, &dwSize) && 1 == dwInstall)
		{
			if (ERROR_SUCCESS == RegQueryValueExW(hkResult, L"SP", 0, 0, (LPBYTE)&dwSP, &dwSize) && (2 == dwSP || 1 == dwSP))
			{
				if (2 == dwSP)
				{
					StringCchCatW(m_info.szDotNetFramework, DOTNET_INFO_LENGTH, L".NET Framework 2.0 Service Pack 2\r\n");
				}
				else
				{
					StringCchCatW(m_info.szDotNetFramework, DOTNET_INFO_LENGTH, L".NET Framework 2.0 Service Pack 1\r\n");
				}
			}
			else
			{
				StringCchCatW(m_info.szDotNetFramework, DOTNET_INFO_LENGTH, L".NET Framework 2.0\r\n");
			}
		}
		RegCloseKey(hkResult);
	}

	//��� HKLM\SOFTWARE\Microsoft\NET Framework Setup\NDP\v3.0\Install - 1
	//         if(HKLM\SOFTWARE\Microsoft\NET Framework Setup\NDP\v3.0\SP - 2)
	//             => .NET Framework 3.0 Service Pack 2
	//         else if(HKLM\SOFTWARE\Microsoft\NET Framework Setup\NDP\v3.0\SP - 1)
	//             => .NET Framework 3.0 Service Pack 1
	//         else
	//             => .NET Framework 3.0
	if (ERROR_SUCCESS == RegOpenKeyExW(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v3.0", 0, KEY_READ, &hkResult))
	{
		dwSize = sizeof(dwInstall);
		if (ERROR_SUCCESS == RegQueryValueExW(hkResult, L"Install", 0, 0, (LPBYTE)&dwInstall, &dwSize) && 1 == dwInstall)
		{
			if (ERROR_SUCCESS == RegQueryValueExW(hkResult, L"SP", 0, 0, (LPBYTE)&dwSP, &dwSize) && (2 == dwSP || 1 == dwSP))
			{
				if (2 == dwSP)
				{
					StringCchCatW(m_info.szDotNetFramework, DOTNET_INFO_LENGTH, L".NET Framework 3.0 Service Pack 2\r\n");
				}
				else
				{
					StringCchCatW(m_info.szDotNetFramework, DOTNET_INFO_LENGTH, L".NET Framework 3.0 Service Pack 1\r\n");
				}
			}
			else
			{
				StringCchCatW(m_info.szDotNetFramework, DOTNET_INFO_LENGTH, L".NET Framework 3.0\r\n");
			}
		}
		RegCloseKey(hkResult);
	}

	//��� HKLM\SOFTWARE\Microsoft\NET Framework Setup\NDP\v3.5\Install - 1
	//         if(HKLM\SOFTWARE\Microsoft\NET Framework Setup\NDP\v3.5\SP - 1)
	//             => .NET Framework 3.5 Service Pack 1
	//         else
	//             => .NET Framework 3.5
	if (ERROR_SUCCESS == RegOpenKeyExW(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v3.5", 0, KEY_READ, &hkResult))
	{
		dwSize = sizeof(dwInstall);
		if (ERROR_SUCCESS == RegQueryValueExW(hkResult, L"Install", 0, 0, (LPBYTE)&dwInstall, &dwSize) && 1 == dwInstall)
		{
			if (ERROR_SUCCESS == RegQueryValueExW(hkResult, L"SP", 0, 0, (LPBYTE)&dwSP, &dwSize) && 1 == dwSP)
			{
				StringCchCatW(m_info.szDotNetFramework, DOTNET_INFO_LENGTH, L".NET Framework 3.5 Service Pack 1\r\n");
			}
			else
			{
				StringCchCatW(m_info.szDotNetFramework, DOTNET_INFO_LENGTH, L".NET Framework 3.5\r\n");
			}
		}
		RegCloseKey(hkResult);
	}

	//��� HKLM\SOFTWARE\Microsoft\NET Framework Setup\NDP\v4.0\Client\Install - 1
	//             => .NET Framework 4.0 Client Profile
	if (ERROR_SUCCESS == RegOpenKeyExW(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4.0\\Client", 0, KEY_READ, &hkResult))
	{
		dwSize = sizeof(dwInstall);
		if (ERROR_SUCCESS == RegQueryValueExW(hkResult, L"Install", 0, 0, (LPBYTE)&dwInstall, &dwSize) && 1 == dwInstall)
		{
			StringCchCatW(m_info.szDotNetFramework, DOTNET_INFO_LENGTH, L".NET Framework 4.0 Client Profile\r\n");
		}
		RegCloseKey(hkResult);
	}

	//��� HKLM\SOFTWARE\Microsoft\NET Framework Setup\NDP\v4.0\Full\Install - 1
	//             => .NET Framework 4.0 Full
	if (ERROR_SUCCESS == RegOpenKeyExW(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4.0\\Full", 0, KEY_READ, &hkResult))
	{
		dwSize = sizeof(dwInstall);
		if (ERROR_SUCCESS == RegQueryValueExW(hkResult, L"Install", 0, 0, (LPBYTE)&dwInstall, &dwSize) && 1 == dwInstall)
		{
			StringCchCatW(m_info.szDotNetFramework, DOTNET_INFO_LENGTH, L".NET Framework 4.0 Full\r\n");
		}
		RegCloseKey(hkResult);
	}

	//��� HKLM\SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Client\Install - 1
	//         switch(HKLM\SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Client\Release)
	//             => .NET Framework 4.5/4.5.1/4.5.2/4.6/4.6.1 Client Profile
	if (ERROR_SUCCESS == RegOpenKeyExW(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4\\Client", 0, KEY_READ, &hkResult))
	{
		dwSize = sizeof(dwInstall);
		if (ERROR_SUCCESS == RegQueryValueExW(hkResult, L"Install", 0, 0, (LPBYTE)&dwInstall, &dwSize) && 1 == dwInstall)
		{
			if (ERROR_SUCCESS == RegQueryValueExW(hkResult, L"Release", 0, 0, (LPBYTE)&dwRelease, &dwSize))
			{
				StringCchCatW(m_info.szDotNetFramework, DOTNET_INFO_LENGTH, GetFxVersionGreaterThen4dot5(dwRelease));
				StringCchCatW(m_info.szDotNetFramework, DOTNET_INFO_LENGTH, L" Client Profile\r\n");
			}
		}
		RegCloseKey(hkResult);
	}

	//��� HKLM\SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full\Install - 1
	//         switch(HKLM\SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full\Release)
	//             => .NET Framework 4.5/4.5.1/4.5.2/4.6/4.6.1 Full
	if (ERROR_SUCCESS == RegOpenKeyExW(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full", 0, KEY_READ, &hkResult))
	{
		dwSize = sizeof(dwInstall);
		if (ERROR_SUCCESS == RegQueryValueExW(hkResult, L"Install", 0, 0, (LPBYTE)&dwInstall, &dwSize) && 1 == dwInstall)
		{
			if (ERROR_SUCCESS == RegQueryValueExW(hkResult, L"Release", 0, 0, (LPBYTE)&dwRelease, &dwSize))
			{
				StringCchCatW(m_info.szDotNetFramework, DOTNET_INFO_LENGTH, GetFxVersionGreaterThen4dot5(dwRelease));
				StringCchCatW(m_info.szDotNetFramework, DOTNET_INFO_LENGTH, L" Full\r\n");
			}
		}
		RegCloseKey(hkResult);
	}
}

void CSysInfo::GetOperatingSystem()
{
	OSVERSIONINFOEX osvi;
	SYSTEM_INFO si;
	PGNSI pGNSI;
	PGPI pGPI;
	BOOL bOsVersionInfoEx;
	DWORD dwType;

	ZeroMemory(&si, sizeof(SYSTEM_INFO));
	ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));

	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
	bOsVersionInfoEx = GetVersionEx((OSVERSIONINFO*)&osvi);

	m_info.iWindowsVer = osvi.dwMajorVersion * 10 + osvi.dwMinorVersion;
	m_info.iServicePackVer = osvi.wServicePackMajor * 10 + osvi.wServicePackMinor;

	LPWSTR pszOS = m_info.szOperatingSystem;

	pGNSI = (PGNSI)GetProcAddress(
		GetModuleHandle(L"kernel32.dll"),
		"GetNativeSystemInfo");
	if (NULL != pGNSI)
		pGNSI(&si);
	else
		GetSystemInfo(&si);

	if (VER_PLATFORM_WIN32_NT == osvi.dwPlatformId &&
		osvi.dwMajorVersion > 4)
	{
		StringCchCopyW(pszOS, WINDOWS_INFO_LENGTH, L"Microsoft ");

		//Windows Vista / Server 2008��֮��汾
		if (osvi.dwMajorVersion == 6)
		{
			if (osvi.dwMinorVersion == 0)
			{
				if (osvi.wProductType == VER_NT_WORKSTATION)
					StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows Vista ");
				else StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows Server 2008 ");
			}

			if (osvi.dwMinorVersion == 1)
			{
				if (osvi.wProductType == VER_NT_WORKSTATION)
					StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows 7 ");
				else StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows Server 2008 R2 ");
			}

			if (osvi.dwMinorVersion >= 2) { //Windows 8/8.1/10
				HKEY hkResult;
				if (ERROR_SUCCESS == RegOpenKeyExW(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion", 0, KEY_READ, &hkResult))
				{
					DWORD dwMajorVer;
					DWORD dwMinorVer;
					DWORD dwSize = sizeof(dwMajorVer);

					//Win10 Or Greater��
					BOOL isWin10OrGreater = false;
					if (ERROR_SUCCESS == RegQueryValueExW(hkResult, L"CurrentMajorVersionNumber", 0, 0, (LPBYTE)&dwMajorVer, &dwSize)) {
						DWORD dwSize = sizeof(dwMinorVer);
						if (ERROR_SUCCESS == RegQueryValueExW(hkResult, L"CurrentMinorVersionNumber", 0, 0, (LPBYTE)&dwMinorVer, &dwSize)) {
							if (dwMajorVer == 10) {
								if (osvi.wProductType == VER_NT_WORKSTATION) {
									StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows 10 ");
								}
								else {
									StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows Server 2016 ");
								}
								isWin10OrGreater = true;
							}
							
							if(dwMajorVer>10){
								StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows 10+? ");
								isWin10OrGreater = true;
							}
						}
					}

					//Win 8/8.1
					if (!isWin10OrGreater) {
						WCHAR version[10];
						dwSize = sizeof(version);
						if (ERROR_SUCCESS == RegQueryValueExW(hkResult, L"CurrentVersion", 0, 0, (LPBYTE)version, &dwSize)) {
							if (version[0] == L'6') {
								if (version[2] == L'2') {
									if (osvi.wProductType == VER_NT_WORKSTATION) {
										StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows 8 ");
									}
									else {
										StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows Server 2012 ");
									}
								}
								else if (version[2] == L'3') {
									if (osvi.wProductType == VER_NT_WORKSTATION) {
										StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows 8.1 ");
									}
									else {
										StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows Server 2012 R2 ");
									}
								}
								else {
									StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows 8.1+? ");
								}
							}
						}
					}

					RegCloseKey(hkResult);
				}
			}

			pGPI = (PGPI)GetProcAddress(
				GetModuleHandle(L"kernel32.dll"),
				"GetProductInfo");

			pGPI(osvi.dwMajorVersion, osvi.dwMinorVersion, 0, 0, &dwType);

			StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"[ ");

			switch (dwType)
			{
			case  PRODUCT_BUSINESS: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Business"); break;
			case  PRODUCT_BUSINESS_N: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Business N"); break;
			case  PRODUCT_CLUSTER_SERVER: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"HPC Edition"); break;
			case  PRODUCT_CLUSTER_SERVER_V: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Server Hyper Core V"); break;
			case  PRODUCT_CORE: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Home"); break;
			case  PRODUCT_CORE_COUNTRYSPECIFIC: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Home China"); break;
			case  PRODUCT_CORE_N: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Home N"); break;
			case  PRODUCT_CORE_SINGLELANGUAGE: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Home Single Language"); break;
			case  PRODUCT_DATACENTER_EVALUATION_SERVER: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Server Datacenter (evaluation installation)"); break;
			case  PRODUCT_DATACENTER_SERVER: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Server Datacenter (full installation)"); break;
			case  PRODUCT_DATACENTER_SERVER_CORE: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Server Datacenter (core installation)"); break;
			case  PRODUCT_DATACENTER_SERVER_CORE_V: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Server Datacenter without Hyper-V (core installation)"); break;
			case  PRODUCT_DATACENTER_SERVER_V: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Server Datacenter without Hyper-V (full installation)"); break;
			case  PRODUCT_ENTERPRISE: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Enterprise"); break;
			case  PRODUCT_ENTERPRISE_E: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Enterprise E"); break;
			case  PRODUCT_ENTERPRISE_EVALUATION: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Enterprise Evaluation"); break;
			case  PRODUCT_ENTERPRISE_N: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Enterprise N"); break;
			case  PRODUCT_ENTERPRISE_N_EVALUATION: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Enterprise N Evaluation"); break;
			case  PRODUCT_ENTERPRISE_SERVER: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Server Enterprise (full installation)"); break;
			case  PRODUCT_ENTERPRISE_SERVER_CORE: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Server Enterprise (core installation)"); break;
			case  PRODUCT_ENTERPRISE_SERVER_CORE_V: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Server Enterprise without Hyper-V (core installation)"); break;
			case  PRODUCT_ENTERPRISE_SERVER_IA64: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Server Enterprise for Itanium-based Systems"); break;
			case  PRODUCT_ENTERPRISE_SERVER_V: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Server Enterprise without Hyper-V (full installation)"); break;
			case  PRODUCT_ESSENTIALBUSINESS_SERVER_ADDL: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows Essential Server Solution Additional"); break;
			case  PRODUCT_ESSENTIALBUSINESS_SERVER_ADDLSVC: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows Essential Server Solution Additional SVC"); break;
			case  PRODUCT_ESSENTIALBUSINESS_SERVER_MGMT: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows Essential Server Solution Management"); break;
			case  PRODUCT_ESSENTIALBUSINESS_SERVER_MGMTSVC: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows Essential Server Solution Management SVC"); break;
			case  PRODUCT_HOME_BASIC: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Home Basic"); break;
			case  PRODUCT_HOME_BASIC_N: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Home Basic N"); break;
			case  PRODUCT_HOME_PREMIUM: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Home Premium"); break;
			case  PRODUCT_HOME_PREMIUM_N: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Home Premium N"); break;
			case  PRODUCT_HOME_PREMIUM_SERVER: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows Home Server 2011"); break;
			case  PRODUCT_HOME_SERVER: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows Storage Server 2008 R2 Essentials"); break;
			case  PRODUCT_HYPERV: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Microsoft Hyper-V Server"); break;
			case  PRODUCT_MEDIUMBUSINESS_SERVER_MANAGEMENT: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows Essential Business Server Management Server"); break;
			case  PRODUCT_MEDIUMBUSINESS_SERVER_MESSAGING: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows Essential Business Server Messaging Server"); break;
			case  PRODUCT_MEDIUMBUSINESS_SERVER_SECURITY: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows Essential Business Server Security Server"); break;
			case  PRODUCT_MOBILE_CORE: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Mobile"); break;
			case  PRODUCT_MULTIPOINT_PREMIUM_SERVER: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows MultiPoint Server Premium (full installation)"); break;
			case  PRODUCT_MULTIPOINT_STANDARD_SERVER: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows MultiPoint Server Standard (full installation)"); break;
			case  PRODUCT_PROFESSIONAL: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Professional"); break;
			case  PRODUCT_PROFESSIONAL_N: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Professional N"); break;
			case  PRODUCT_PROFESSIONAL_WMC: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Professional with Media Center"); break;
			case  PRODUCT_SB_SOLUTION_SERVER: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows Small Business Server 2011 Essentials"); break;
			case  PRODUCT_SB_SOLUTION_SERVER_EM: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Server For SB Solutions EM"); break;
			case  PRODUCT_SERVER_FOR_SB_SOLUTIONS: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Server For SB Solutions"); break;
			case  PRODUCT_SERVER_FOR_SB_SOLUTIONS_EM: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Server For SB Solutions EM"); break;
			case  PRODUCT_SERVER_FOR_SMALLBUSINESS: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows Server 2008 for Windows Essential Server Solutions"); break;
			case  PRODUCT_SERVER_FOR_SMALLBUSINESS_V: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows Server 2008 without Hyper-V for Windows Essential Server Solutions"); break;
			case  PRODUCT_SERVER_FOUNDATION: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Server Foundation"); break;
			case  PRODUCT_SMALLBUSINESS_SERVER: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows Small Business Server"); break;
			case  PRODUCT_SMALLBUSINESS_SERVER_PREMIUM: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Small Business Server Premium"); break;
			case  PRODUCT_SMALLBUSINESS_SERVER_PREMIUM_CORE: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Small Business Server Premium (core installation)"); break;
			case  PRODUCT_SOLUTION_EMBEDDEDSERVER: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows MultiPoint Server"); break;
			case  PRODUCT_STANDARD_EVALUATION_SERVER: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Server Standard (evaluation installation)"); break;
			case  PRODUCT_STANDARD_SERVER: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Server Standard"); break;
			case  PRODUCT_STANDARD_SERVER_CORE: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Server Standard (core installation)"); break;
			case  PRODUCT_STANDARD_SERVER_CORE_V: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Server Standard without Hyper-V (core installation)"); break;
			case  PRODUCT_STANDARD_SERVER_V: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Server Standard without Hyper-V"); break;
			case  PRODUCT_STANDARD_SERVER_SOLUTIONS: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Server Solutions Premium"); break;
			case  PRODUCT_STANDARD_SERVER_SOLUTIONS_CORE: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Server Solutions Premium (core installation)"); break;
			case  PRODUCT_STARTER: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Starter"); break;
			case  PRODUCT_STARTER_N: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Starter N"); break;
			case  PRODUCT_STORAGE_ENTERPRISE_SERVER: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Storage Server Enterprise"); break;
			case  PRODUCT_STORAGE_ENTERPRISE_SERVER_CORE: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Storage Server Enterprise (core installation)"); break;
			case  PRODUCT_STORAGE_EXPRESS_SERVER: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Storage Server Express"); break;
			case  PRODUCT_STORAGE_EXPRESS_SERVER_CORE: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Storage Server Express (core installation)"); break;
			case  PRODUCT_STORAGE_STANDARD_EVALUATION_SERVER: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Storage Server Standard (evaluation installation)"); break;
			case  PRODUCT_STORAGE_STANDARD_SERVER: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Storage Server Standard"); break;
			case  PRODUCT_STORAGE_STANDARD_SERVER_CORE: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Storage Server Standard (core installation)"); break;
			case  PRODUCT_STORAGE_WORKGROUP_EVALUATION_SERVER: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Storage Server Workgroup (evaluation installation)"); break;
			case  PRODUCT_STORAGE_WORKGROUP_SERVER: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Storage Server Workgroup"); break;
			case  PRODUCT_STORAGE_WORKGROUP_SERVER_CORE: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Storage Server Workgroup (core installation)"); break;
			case  PRODUCT_ULTIMATE: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Ultimate"); break;
			case  PRODUCT_ULTIMATE_N: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Ultimate N"); break;
			case  PRODUCT_UNDEFINED: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"An unknown product"); break;
			case  PRODUCT_WEB_SERVER: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Web Server (full installation)"); break;
			case  PRODUCT_WEB_SERVER_CORE: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Web Server (core installation)"); break;
			default: StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"(Unknown Edition)"); break;
			}

			StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L" ]");
		}

		//Windows Server 2003
		if (osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 2)
		{
			if (GetSystemMetrics(SM_SERVERR2))
				StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows Server 2003 R2, ");
			else if (osvi.wSuiteMask & VER_SUITE_STORAGE_SERVER)
				StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows Storage Server 2003");
			else if (osvi.wSuiteMask & VER_SUITE_WH_SERVER)
				StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows Home Server");
			else if (osvi.wProductType == VER_NT_WORKSTATION &&
				si.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64)
			{
				StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows XP Professional x64 Edition");
			}
			else StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows Server 2003, ");

			// Test for the server type.
			if (osvi.wProductType != VER_NT_WORKSTATION)
			{
				if (si.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_IA64)
				{
					if (osvi.wSuiteMask & VER_SUITE_DATACENTER)
						StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Datacenter Edition for Itanium-based Systems");
					else if (osvi.wSuiteMask & VER_SUITE_ENTERPRISE)
						StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Enterprise Edition for Itanium-based Systems");
				}

				else if (si.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64)
				{
					if (osvi.wSuiteMask & VER_SUITE_DATACENTER)
						StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Datacenter x64 Edition");
					else if (osvi.wSuiteMask & VER_SUITE_ENTERPRISE)
						StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Enterprise x64 Edition");
					else StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Standard x64 Edition");
				}

				else
				{
					if (osvi.wSuiteMask & VER_SUITE_COMPUTE_SERVER)
						StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Compute Cluster Edition");
					else if (osvi.wSuiteMask & VER_SUITE_DATACENTER)
						StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Datacenter Edition");
					else if (osvi.wSuiteMask & VER_SUITE_ENTERPRISE)
						StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Enterprise Edition");
					else if (osvi.wSuiteMask & VER_SUITE_BLADE)
						StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Web Edition");
					else StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Standard Edition");
				}
			}
		}

		//Windows XP
		if (osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 1)
		{
			StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows XP ");
			if (osvi.wSuiteMask & VER_SUITE_PERSONAL)
				StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Home Edition");
			else StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Professional");
		}

		//Windows 2000
		if (osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 0)
		{
			StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Windows 2000 ");

			if (osvi.wProductType == VER_NT_WORKSTATION)
			{
				StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Professional");
			}
			else
			{
				if (osvi.wSuiteMask & VER_SUITE_DATACENTER)
					StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Datacenter Server");
				else if (osvi.wSuiteMask & VER_SUITE_ENTERPRISE)
					StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Advanced Server");
				else StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Server");
			}
		}

		// Include service pack (if any) and build number.
		size_t stLen = 0;
		StringCchLength(osvi.szCSDVersion, 128, &stLen);
		if (stLen > 0)
		{
			StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L" ");
			StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, osvi.szCSDVersion);
		}
		TCHAR buf[80];
		StringCchPrintf(buf, 80, L" (build %d)", osvi.dwBuildNumber);
		StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, buf);

		//32bit or 64bit
		if (osvi.dwMajorVersion >= 6)
		{
			if (si.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64)
				StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L", 64-bit");
			else if (si.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_INTEL)
				StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L", 32-bit");
		}
	}
	else
	{
		StringCchCatW(pszOS, WINDOWS_INFO_LENGTH, L"Unknown Windows edition.");
	}
}
