//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SysInfoDetector.rc
//
#define IDS_APP_TITLE                   103
#define IDI_SYSINFODETECTOR             107
#define IDC_SYSINFODETECTOR             109
#define IDD_MAIN                        129
#define IDC_EDIT_OPERATING_SYSTEM       1000
#define IDC_EDIT2                       1001
#define IDC_EDIT_DOT_NET_FRAMEWORK      1001
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
