// SysInfoDetector.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "SysInfoDetector.h"
#include "SysInfo.h"

INT_PTR CALLBACK DialogProc(HWND hwndDlg,  // handle to dialog box
	UINT uMsg,     // message
	WPARAM wParam, // first message parameter
	LPARAM lParam  // second message parameter
);

CSysInfo g_sysInfo;

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	DialogBox(hInstance, MAKEINTRESOURCE(IDD_MAIN), NULL, DialogProc);
	return 0;
}


INT_PTR CALLBACK DialogProc(HWND hwndDlg,  // handle to dialog box
	UINT uMsg,     // message
	WPARAM wParam, // first message parameter
	LPARAM lParam  // second message parameter
)
{
	switch (uMsg)
	{
	case WM_INITDIALOG:
	{
		SystemInfoDef* pSI = g_sysInfo.GetAll();
		SetDlgItemTextW(hwndDlg, IDC_EDIT_OPERATING_SYSTEM, pSI->szOperatingSystem);
		SetDlgItemTextW(hwndDlg, IDC_EDIT_DOT_NET_FRAMEWORK, pSI->szDotNetFramework);

		RECT rectDlg;
		int iScrWidth = GetSystemMetrics(SM_CXFULLSCREEN);
		int iScrHeight = GetSystemMetrics(SM_CYFULLSCREEN);
		GetWindowRect(hwndDlg, &rectDlg);
		int iDlgHeight = rectDlg.bottom - rectDlg.top;
		int iDlgWidth = rectDlg.right - rectDlg.left;
		MoveWindow(hwndDlg, (iScrWidth - iDlgWidth) / 2, (iScrHeight - iDlgHeight) / 2, iDlgWidth, iDlgHeight, FALSE);
	}
	break;
	case WM_SYSCOMMAND:
		if ((WPARAM)wParam == SC_CLOSE)
		{
			EndDialog(hwndDlg, LOWORD(wParam));
		}
		break;
	}
	return FALSE;
}
